# README #

### Installation ###

* `$ pip install isub`

### Examples ###

* Simple command  
`$ isub do python your_script.py done -N your_job_name`  

* Multiple commands  
`$ isub do command1\; command2\; command3  done -N your_job_name`

### Contact ###

* Kohei Miyaguchi (koheimiyaguchi at gmail)
